#include "evol.h"
#include "labels.h"
#include "flags.h"

map <string, pt> vlabel;

pt get_label_vertex (string label, bool unbad) {
	if (!vlabel.count (label)) {
		pt v = make_vertex ("", FL_BAD);
		vlabel[label] = v;
	}

	if (unbad && has_flags (vlabel[label], FL_BAD))
		remove_flags (vlabel[label], FL_BAD);

	return vlabel[label];
}

pt make_goto_vertex (pt to) {
	pt v = make_vertex ("");
	connect (v, to);
	add_flags (v, FL_GOTO);
	return v;
}

bool check_undef_labels () {
	bool result = true;
	for (map <string, pt> :: iterator it = vlabel.begin (); it != vlabel.end (); ++it)
		if (has_flags (it->second, FL_BAD)) {
			fprintf (stderr, "warn: label `%s` was never defined\n", it->first.c_str ());
			result = false;
		}
	return result;
}

void clear_labels () {
	vlabel.clear ();
}
