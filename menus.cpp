#include "menus.h"
#include "evol.h"
#include "labels.h"

namespace menust {
	struct menurec {
		bool isfunc;
		map <int, pt> choice;
	};
	map <int, menurec> recs;
	map <pt, pt> connect_after;

	pt last_menuint = def_const;
}

using namespace menust;

void clear_menus () {
	recs.clear ();
	connect_after.clear ();
	last_menuint = def_const;
}

bool is_menup (pt p) {
	if (!is_vertexp (p))
		return false;
	return recs.count (p.first) || recs.count (p.second);
}

bool is_last_menuint_p (pt p) {
	if (!is_vertexp (p))
		return false;
	return last_menuint.first == p.first || last_menuint.second == p.second;
}

void reset_last_menuint () {
	last_menuint = def_const;
}

pt get_last_menuint () {
	return last_menuint;
}

pt get_menu_choice (pt p, int id, bool dont_connect_after) {
	assert (is_menup (p));
	if (!(0 <= id))
		return def_const;

	int pid = p.first;
	if (!recs.count (pid))
		pid = p.second;

	assert (recs.count (pid));

	if (!recs[pid].choice.count (id))
		recs[pid].choice[id] = def_const;

	pt result = recs[pid].choice[id];
	if (dont_connect_after)
		connect_after.erase (result);

	return result;
}

pt process_menu (bool isfunc, const string type, YYSTYPE args) {
	menurec rec;
	rec.isfunc = isfunc;
	rec.choice.clear ();

	pt res = make_vertex ("");
	pt ev = make_vertex ("");

	assert (type == "select" || type == "menuint" || type == "menu");

	int delta = (type == "select" ? 1 : 2);

	for (int i = 0; i < args.size (); i += delta) {
		mark_as_player (args[i]);
		connect (res, args[i]);
		if (type == "menu" && get_constant (args[i + 1]) != "-")
			connect (args[i], get_label_vertex (get_constant (args[i + 1]), false));
		else {
			connect_after[args[i]] = ev;
			int x = -1;
			if (type == "select")
				x = i + 1;
			if (type == "menuint" && i + 1 < args.size ())
				x = get_constant_int (args[i + 1]);
			if (x != -1)
				rec.choice[x] = args[i];
		}
	}

	res.second = ev.second;

	if (type == "select");
		last_menuint = res;

	recs[res.first] = rec;
	recs[res.second] = rec;
	return res;
}

void menus_postprocess () {
	for (map <pt, pt> :: iterator it = connect_after.begin (); it != connect_after.end (); ++it) {
		connect (it->first, it->second);
	}
}
