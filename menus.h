#ifndef _MENUS_H_
#define _MENUS_H_

#include "evol.h"

pt process_menu (bool isfunc, const string type, YYSTYPE args);
bool is_menup (pt p);
pt get_menu_choice (pt p, int id, bool dont_connect_after = false);
void reset_last_menuint ();
bool is_last_menuint_p (pt p);
void menus_postprocess ();
pt get_last_menuint ();
void clear_menus ();

#endif
