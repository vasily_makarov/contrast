#include "evol.h"
#include "funccalls.h"
#include "flags.h"

namespace funcst {
	map <string, pair <pt, pt> > pfunc;
	set <string> fdef;
	pt cur_func_end = def_const;
	map <pt, string> fcalls;
	map <pt, string> fname;
}

using namespace funcst;

void clear_funccalls () {
	pfunc.clear ();
	fdef.clear ();
	cur_func_end = def_const;
	fcalls.clear ();
	fname.clear ();
}

pair <pt, pt> get_func_vertices (const string& func_name) {
	if (!pfunc.count (func_name)) {
		pfunc[func_name].first = make_vertex ("", FL_FUNCV_S | FL_DONTREMOVE);
		pfunc[func_name].second = make_vertex ("", FL_FUNCV_E);
	}

	fname[pfunc[func_name].first] = func_name;
	fname[pfunc[func_name].second] = func_name;

	return pfunc[func_name];
}

pt get_func_start (const string& func_name) {
	return get_func_vertices (func_name).first;
}

pt get_func_end (const string& func_name) {
	return get_func_vertices (func_name).second;
}

void set_current_func (const string& func_name) {
	cur_func_end = get_func_end (func_name);
	if (fdef.count (func_name))
		fprintf (stderr, "warn: function `%s` set current twice\n", func_name.c_str ()); 
	fdef.insert (func_name);
}

pt get_current_func_end () {
	return cur_func_end;
}

void reset_current_func () {
	cur_func_end = def_const;
}

void remove_all_undef_func () {
	map <string, pair <pt, pt> > :: iterator it;
	for (it = pfunc.begin (); it != pfunc.end (); ++it) {
		if (!fdef.count (it->first)) {
			remove_vertex (it->second.first.first, false);
			remove_vertex (it->second.second.first, false);

		}

		if (!has_target_edge (it->second.second)) {
			remove_vertex (it->second.second.first, false);
		}
	}

	for (map <pt, string> :: iterator jt = fcalls.begin (); jt != fcalls.end (); ++jt)
		if (!fdef.count (jt->second))
			remove_vertex (jt->first.first);
}

pt make_func_call (const string& func_name) {
	pt res = make_vertex ("", FL_FCALL | FL_DONTREMOVE);
	fcalls[res] = func_name;
	fname[res] = func_name;
	return res;
}

pt make_ret_vertex () {
	return make_vertex ("", FL_FRET | FL_DONTREMOVE);
}

string get_func_name (const pt& v) {
	return fname.count(v) ? fname[v] : "";
}
