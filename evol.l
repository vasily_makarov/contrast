%option yylineno

%{
#include "evol.h"

#include "y.tab.h"
#include "evol.h"

extern "C" {
	int yylex(void);
}


%}

compare_ops	"<"|"<="|"=="|"!="|">="|">"
logic_ops	"&&"|"||"
arithm_ops	"+"|"-"|"/"|"*"|"|"|"&"
inc_ops		"++"|"--"
assign_ops	"="|"*="|"/="|"+="|"-="
string		\"(\\.|[^\\"])*\"
script_def	\tscript\t(\\.|[^\\"])*\t
whitespaces	"\n"|"\r"|" "
identifier	[\$@\._a-zA-Z][\$@\._a-zA-Z0-9]*
hexnum		0[xX][0-9a-fA-F]+
tab			"\t"

%%
"//".*
[.]*		yylval = def_yystype;
{hexnum}	{
			yylval = def_yystype;
			yylval[0] = make_constant (yytext);
			return NUMBER;
		}
[0-9]+	{
			yylval = def_yystype;
			yylval[0] = make_constant (yytext);
			return NUMBER;
		}
{inc_ops}	return INC_OP;
"-"		return MINUS_OP;
"?"		return QSTMARK;
":"		return COLON;
";"		return SEMICOLON;
{assign_ops}	return ASSIGN_OP;
"("		return OBRACE;
")"		return EBRACE;
"{"		return OCBRACE;
"}"		return ECBRACE;
","		return COMMA;
{compare_ops}	return COMPARE_OP;
{logic_ops}	return LOGIC_OP;
{arithm_ops}	return ARITHM_OP;
{string}	{	
			yylval = def_yystype;
			yylval[0] = make_constant (yytext, yylineno);
			return STRING_CONST;
		}
"if"		return IF_KEYWD;
"else"		return ELSE_KEYWD;
"function"	return FUNCTION_KEYWD;
"while"		return WHILE_KEYWD;
"do"		return DO_KEYWD;
"switch"	return SWITCH_KEYWD;
"case"		return CASE_KEYWD;
"default"	return DEFAULT_KEYWD;
"for"		return FOR_KEYWD;
"!"		return LOGIC_NEGATE;
{identifier}	{
			yylval = def_yystype;
			yylval[0] = make_constant (yytext);
			return IDENTIFIER;
		}
{script_def} {
			yylval = def_yystype;

			char *ptr = yytext + 1;
			while (*ptr != '\t')
				++ptr;

			string s = ptr + 1;
			assert (s.length () > 0);
			s = s.substr (0, s.length () - 1);

			yylval[0] = make_constant (s);
			return SCRIPT_DEF;
		}
{whitespaces}
%%
