#include "evol.h"
#include "y.tab.h"
#include "breaks.h"
#include "funccalls.h"
#include "labels.h"
#include "flags.h"
#include "classes.h"
#include "menus.h"

#include <sys/types.h>
#include <sys/stat.h>

const int N = 1000 * 1000;
vector <string> vc;
int vl[N];
vector <int> g[N], gt[N];
pt def_const;
pt end_vertex;
YYSTYPE def_yystype;

string script_name;

int cc;
string cvalue[N];
int cline[N];

pt make_constant (string s, int lineno) {	
	cline[cc] = lineno;
	cvalue[cc++] = s;
	return pt (-1, cc - 1);
}

pt make_vertex (string caption, int flags, int lineno) {
	vc.push_back (caption);
	vl[vc.size () - 1] = lineno;
	set_flags (vc.size () - 1, flags);
	return pt (vc.size () - 1, vc.size() - 1);
}

void make_edge (int u, int v) {
	g[u].push_back (v);
	gt[v].push_back (u);
}

bool is_vertexp (const pt& x) {
	return 0 <= x.first && x.first < vc.size() &&
		0 <= x.second && x.second < vc.size();
}

pt connect (const pt& f, const pt& s) {
	if (!is_vertexp (f) && !is_vertexp (s))
		return def_const;

	if (!is_vertexp (s))
		return f;

	if (!is_vertexp (f))
		return s;

	if (!has_flags (f.second, FL_GOTO))
		make_edge (f.second, s.first);

	return pt (f.first, s.second);
}

bool is_constant (const pt &x) {
	return x.first == -1 && 0 <= x.second && x.second < cc;
}

string get_constant (const pt &x, int *lineno) {
	if (!is_constant (x)) {
		fprintf (stderr, "warn: get_constant called from non-constant elem\n");
		return "";
	}

	if (lineno != NULL)
		*lineno = cline[x.second];

	return cvalue[x.second];
}

int get_constant_int (const pt &x) {
	string s = get_constant (x);

	if (s == "")
		return -1;

	if (s.substr (0, 2) == "0x") {
		// TODO process hexadecimal
		return -1;
	} else {
		int res = 0;
		for (int i = 0; i < s.size (); ++i) {
			if (!('0' <= s[i] && s[i] <= '9'))
				return -1;
			res = res * 10 + int (s[i] - '0');
		}
		return res;
	}
}

void normalize (vector <int> &v) {
	sort (v.begin (), v.end ());
	v.erase (unique (v.begin (), v.end ()), v.end ());
}

void remove_vertex (int v, bool save_edges) {
	add_flags (v, FL_REMOVED);

	for (int i = 0; i < vc.size (); ++i) {
		if (has_flags (i, FL_REMOVED))
			continue;

		normalize (g[i]);
		normalize (gt[i]);

		for (int j = 0; j < g[i].size (); ++j)
			if (g[i][j] == v) {
				if (save_edges) {
					for (int k = 0; k < g[v].size (); ++k) {
						int rep = g[v][k];
						g[i].push_back (rep);
						gt[rep].push_back (i);
					}
				}

				g[i].erase (find (g[i].begin (), g[i].end (), v));

				break;
			}

		if (find (gt[i].begin (), gt[i].end (), v) != gt[i].end ())
			gt[i].erase (find (gt[i].begin (), gt[i].end (), v));
	}

	g[v].clear ();
	gt[v].clear ();
}

int used[N], cused;

void dfs_mark (int v, int flags) {
	if (used[v] == cused)
		return;
	used[v] = cused;

	if (vc[v] != "" && !has_flags (v, FL_SHOWN))
		add_flags (v, flags | FL_SHOWN);

	for (int i = 0; i < g[v].size (); ++i)
		dfs_mark (g[v][i], flags);
}

void mark_shown (pt v, int flags) {
	cused++;
	dfs_mark (v.first, flags);
}

void mark_as_npc (pt v) {
	cused++;
	mark_shown (v, FL_NPC);
}

void mark_as_player (pt v) {
	cused++;
	mark_shown (v, FL_PLAYER);
}

void mark_as_narrator (pt v) {
	cused++;
	mark_shown (v, FL_NARRATOR);
}

pt get_end_vertex () {
	return end_vertex;
}

bool is_skipping (int v) {
	for (int i = 0; i < vc.size (); ++i) {
		if (i == v)
			continue;
		int cntd = 0, cntnd = 0;
		bool fv = false;
		for (int j = 0; j < g[i].size (); ++j) {
			int to = g[i][j];
			if (vc[to] != "")
				cntd++;
			else
				cntnd++;
			fv |= (to == v);
		}

		if (cntnd == 1 && fv && cntd)
			return true;
	}
	return false;
}

bool has_target_edge (pt v) {
	return !gt[v.first].empty ();
}

pt start_vertex;

pt get_start_vertex () {
	return start_vertex;
}


void clear () {
	for (int i = 0; i < vc.size (); ++i) {
		g[i].clear ();
		gt[i].clear ();
	}

	vc.clear ();

	clear_menus ();
	clear_labels ();
	clear_funccalls ();
	clear_flags ();
	clear_breaks ();

	start_vertex = make_vertex ("", FL_DONTREMOVE);
	def_const = make_constant ("");
	def_yystype = YYSTYPE (1, def_const);
	end_vertex = make_vertex ("");
}

int filen = 0;

void finalize_script () {
	filen++;
	remove_all_undef_func ();
	check_undef_labels ();
	menus_postprocess ();

	bool any;

	do {
		any = false;
		for (int i = 0; i < vc.size (); ++i) {
			if (has_any_flag (i, FL_REMOVED | FL_DONTREMOVE))
				continue;
			if (vc[i] != "")
				continue;

			normalize (g[i]);
			normalize (gt[i]);

			if (g[i].size () == 1 && g[i][0] == i)
				g[i].clear (), gt[i].erase (find (gt[i].begin (), gt[i].end (), i));

			if (
				(gt[i].size () == 1 && gt[i][0] != i) ||
				(g[i].size () == 1 && g[i][0] != i) ||
				(g[i].size () == 0 && !is_skipping (i))
			) {
				remove_vertex (i), any = true;
				continue;
			}
		}
	} while (any);

	int vcount = 0;
	for (int i = 0; i < vc.size(); ++i) {
		if (has_flags (i, FL_REMOVED))
			continue;

		vcount++;
	}

	if (vcount == 1) {
		clear ();
		return;
	}

	fprintf (stderr, "saving script %s...\n", script_name.c_str ());

	string _filen = "";
	int x = filen;
	while (x) {
		_filen += char ('0' + x % 10);
		x /= 10;
	}

	string fname = string ("nodes/" + _filen);
	mkdir (fname.c_str (), S_IRWXU);

	freopen(string (fname + "/scriptname").c_str (), "w", stdout);
	puts (script_name.c_str ());

	freopen(string (fname + "/nodes.js").c_str (), "w", stdout);
	puts ("var _cy_nodes = [");
	for (int i = 0; i < vc.size(); ++i) {
		if (has_flags (i, FL_REMOVED))
			continue;

		vcount++;

		if (vc[i] != "" && !has_flags (i, FL_SHOWN)) {
			fprintf (stderr, "warn: string `%s` is not shown (or it's not recognized)\n", vc[i].c_str ());
			add_flags (i, FL_NOTSHOWN);
		}

		printf ("%s", get_params (pt (i, i), vc[i] == "" ? "''" : vc[i].c_str (), vl[i]).c_str () );

		printf ("},\n");
	}
	puts ("];");

	puts ("var _cy_edges = [");
	for (int i = 0; i < vc.size(); ++i)
		if (!has_flags (i, FL_REMOVED)) {
			for (int j = 0; j < g[i].size(); ++j) {
				int to = g[i][j];
				printf("\t{data: {source: 'v%d', target: 'v%d'}},\n", i, to);
			}
		}

	puts ("];");

	printf ("_script_name = '%s';\n", script_name.c_str ());
	clear ();

}

int main()
{
	clear ();
	yydebug = 0;
	yyparse();

	return 0;
} 
