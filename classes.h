#ifndef _CLASSES_H_
#define _CLASSES_H_

#include "evol.h"

string get_classes (pt v);
string get_params (const pt& v, const string& datastr, int lineno);

#endif
