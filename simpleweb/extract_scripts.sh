#!/bin/bash

rm -rf html
cp -r ../html .

rm -rf result nodes
mkdir result nodes
i=0

echo "<html><body><table>" > result/index.html

dir=$1/npc

for s in `cd "$dir" && find */ -name '*.txt' ! -name '_*.txt' ! -name 'doors.txt' ! -name 'mapflags.txt' `
do
	rm -rf nodes/*
	t=$s
	../y < "$dir/$s" > /dev/null 2> /dev/null
	if [ $? == 0 ]
	then
		echo $s
		for u in `cd nodes && find . -mindepth 1 -maxdepth 1 -type d -printf '%f\n' ! -name '.'`
		do
			w=`cat nodes/$u/scriptname`
			echo $w
			rm -rf result/$i
			mkdir result/$i
			cp html/* result/$i
			cp "nodes/$u/nodes.js" result/$i
			echo "<tr><td>$t</td><td><a href=\"$i/index.html\">$w</a></td></tr>" >> result/index.html
			echo "_script_path = '$s';" >> result/$i/nodes.js
			i=$((i+1))
			t=
		done
	fi
done

echo "</table></body></html>" >> result/index.html

rm -rf html nodes
