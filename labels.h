#ifndef _LABELS_H_
#define _LABELS_H_

#include "evol.h"

pt get_label_vertex (string label, bool unbad);
pt make_goto_vertex (pt to);
bool check_undef_labels ();
void clear_labels ();

#endif
