#include "evol.h"
#include "breaks.h"

stack <pt> stbreak;

pt top_break_vertex () {
	if (stbreak.empty ())
		return def_const;
	return stbreak.top ();
}

pt pop_break_vertex () {
	pt res = top_break_vertex ();
	stbreak.pop ();
	return res;
}

void push_break_vertex () {
	stbreak.push (make_vertex (""));
}

void clear_breaks () {
	while (!stbreak.empty ())
		stbreak.pop ();
}
