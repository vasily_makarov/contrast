#ifndef _FUNCCALLS_H_
#define _FUNCCALLS_H_

#include "evol.h"

void clear_funccalls ();

pt get_func_start (const string& func_name);
pt get_func_end (const string& func_name);

void set_current_func (const string& func_name);
pt get_current_func_end ();
void reset_current_func ();

void remove_all_undef_func ();
pt make_func_call (const string& func_name);
pt make_ret_vertex ();

string get_func_name (const pt& v);

#endif
