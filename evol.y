%{

#define YYDEBUG 1
#include "evol.h"
#include "funccalls.h"
#include "breaks.h"
#include "labels.h"
#include "flags.h"
#include "menus.h"

extern int yyparse(void);
extern "C" {
	int yylex(void);  
	int yywrap() {
		return 1;
	}
}
 
void yyerror(const char *str) {
	fprintf(stderr,"error: %s\n",str);
	exit (1);
} 
extern int yydebug;

%}

%token NUMBER COMPARE_OP LOGIC_OP EBRACE OBRACE
%token ECBRACE OCBRACE ARITHM_OP STRING_CONST
%token IF_KEYWD ELSE_KEYWD COMMA ASSIGN_OP
%token IDENTIFIER LOGIC_NEGATE SEMICOLON
%token FUNCTION_KEYWD SWITCH_KEYWD WHILE_KEYWD DO_KEYWD FOR_KEYWD
%token COLON CASE_KEYWD DEFAULT_KEYWD MINUS_OP INC_OP DEC_OP
%token QSTMARK SCRIPT_DEF

%start input

%%
map_position_prefix:
	arithm_expr COMMA arithm_expr COMMA arithm_expr COMMA arithm_expr {}
npc_props_suffix:
	IDENTIFIER COMMA {}
|	IDENTIFIER COMMA arithm_expr COMMA arithm_expr COMMA {}
script:
	map_position_prefix SCRIPT_DEF npc_props_suffix braced_lines
	{
		connect (get_start_vertex (), $4[0]);
		script_name = get_constant ($2[0]);
	}
;
input:
	{}
|	input script
	{
		finalize_script ();
	}
;
lines:
	/* empty */	{ $$ = def_yystype; }
|	lines line
	{
		assert ($1.size () && $2.size ());
		$$ = def_yystype;
		$$[0] = connect ($1[0], $2[0]);

		if (is_last_menuint_p ($1[0]))
			reset_last_menuint ();
	}
;
braced_lines:
	OCBRACE lines ECBRACE	{ $$ = $2; }
;
if_stmt:
	IF_KEYWD OBRACE logic_expr EBRACE body
	{
		$$ = def_yystype;

		pt sv = make_vertex ("");
		pt ev = make_vertex ("");

		$$[0] = connect (sv, $3[0]);
		$$[0] = connect ($$[0], $5[0]);
		connect ($5[0], ev);
		$$[0] = connect (sv, ev);
	}
|	IF_KEYWD OBRACE logic_expr EBRACE body ELSE_KEYWD body
	{
		$$ = def_yystype;
		pt sv = make_vertex ("");
		pt ev = make_vertex ("");

		$$[0] = connect (sv, $3[0]);
		connect ($$[0], $7[0]);
		$$[0] = connect ($$[0], $5[0]);

		if (is_vertexp ($7[0]))
			connect ($7[0], ev);
		else
			connect (sv, ev);

		$$[0] = connect ($$[0], ev);
	}
;
body:
	line		{ $$ = $1; }
|	braced_lines	{ $$ = $1; }
;
logic_expr:
	logic_single			{ $$ = $1; }
|	LOGIC_NEGATE braced_logic_expr	{ $$ = $2; }
|	logic_expr LOGIC_OP logic_expr	{ $$ = $1; /* TODO */ }
|	braced_logic_expr		{ $$ = $1; }
|	logic_expr QSTMARK logic_expr COLON logic_expr { $$ = $1; /* TODO */ }
;
logic_single:
	arithm_expr				{ $$ = $1; }
|	arithm_expr COMPARE_OP arithm_expr	{ $$ = $1; /* TODO */ }
|	LOGIC_NEGATE arithm_single		{ $$ = $2; }
;
arithm_op:
	ARITHM_OP
|	MINUS_OP
;
arithm_expr:
	arithm_single				{ $$ = $1; }
|	arithm_expr arithm_op arithm_expr	{ $$ = $1; /* TODO */ }
|	OBRACE arithm_expr EBRACE		{ $$ = $2; }
;
arithm_single:
	NUMBER			{ $$ = $1; }
|	MINUS_OP NUMBER	{ $$ = $2; /* TODO */ }
|	STRING_CONST		{ $$ = $1; }
|	IDENTIFIER		{ $$ = $1; }
|	func_call		{ $$ = $1; }
|	inc_expr		{ $$ = $1; }
inc_expr:
	IDENTIFIER INC_OP	{ $$ = $1; }
|	IDENTIFIER DEC_OP	{ $$ = $1; }
;
func_call:
	IDENTIFIER OBRACE proc_args EBRACE
	{
		$$ = def_yystype;

		string func_name = get_constant ($1[0]);
		if (func_name == "selectd")
			func_name = "select";

		for (int i = 0; i < $3.size (); ++i) {
			if (!is_vertexp ($3[i]))
				continue;
			if (func_name == "select")
				mark_as_player ($3[i]);
			if (func_name == "menuaction")
				mark_as_narrator ($3[i]);
		}
				

		if (func_name == "l") {
			string s = "";
			int lineno;
			if (!$3.empty () && is_constant ($3[0])) {
				s = get_constant ($3[0], &lineno);
			}

			if (!s.empty () && s[0] == '"')
				$$[0] = make_vertex (s, 0, lineno);
			else
				fprintf (stderr, "warn: first argument of `l` is not a string constant\n");
		} else
		
		if (func_name == "lg") {
			pt sv = make_vertex ("");
			pt ev = make_vertex ("");

			int lineno;
			string s = get_constant ($3[0], &lineno);

			if ($3.size () < 2) {
				s.insert (s.end () - 1, '#');
				s.insert (s.end () - 1, '0');
			}

			$$[0] = connect (sv, make_vertex (s, 0, lineno));
			$$[0] = connect ($$[0], ev);

			if ($3.size () < 2)
				s[s.size () - 2] = '1';
			else
				s = get_constant ($3[1]);

			$$[0] = connect (sv, make_vertex (s, 0, lineno));
			$$[0] = connect ($$[0], ev);
		} else

		if (func_name == "g") {
			pt sv = make_vertex ("");
			pt ev = make_vertex ("");

			$$[0] = connect (sv, $3[0]);
			$$[0] = connect ($$[0], ev);
			$$[0] = connect (sv, $3[1]);
			$$[0] = connect ($$[0], ev);
		} else

		if (func_name == "rif") {
			if ($3.size() >= 2) {
				if (!is_vertexp ($3[0]) && $3.size () == 2) {
					$$[0] = $3[1];
				} else {
					pt c = $3[0];
					if (!is_vertexp (c))
						c = make_vertex ("");
					pt u = make_vertex ("");

					bool anynvp = false;

					for (int i = 1; i < min (3, (int) $3.size ()); ++i) {
						connect (c, $3[i]);
						connect ($3[i], u);
						anynvp |= !is_vertexp ($3[i]);
					}

					if (anynvp)
						$$[0] = connect (c, u);
					else
						$$[0] = pt (c.first, u.second);
				} } else fprintf (stderr, "warn: `rif` has less than two arguments\n");
		} else

		if (func_name == "select") {
			$$[0] = process_menu (true, func_name, $3);
/*			if ($3.size ()) {
				$$[0] = make_vertex ("");
				pt ev = make_vertex ("");
				for (int i = 1; i < $3.size (); ++i) {
					connect ($$[0], $3[i]);
					connect ($3[i], ev);
				}

				$$[0] = connect ($$[0], $3[0]);
				$$[0] = connect ($$[0], ev);
			}*/
		} else

		{
			for (int i = 0; i < $3.size (); ++i) {
				$$[0] = connect ($$[0], $3[i]);
			}

			$$[0] = connect ($$[0], make_func_call (func_name));
		}
	}
;
proc_args:
	/* empty */			{ $$ = YYSTYPE (); }
|	MINUS_OP
	{
		$$ = def_yystype;
		$$[0] = make_constant ("-");
	}
|	logic_expr			{ $$ = $1; }
|	proc_args COMMA proc_args
	{
		$$ = $1;
		$$.insert ($$.end (), $3.begin (), $3.end ());
	}
;
line:
	assign_expr SEMICOLON		{ $$ = $1; }
|	inc_expr SEMICOLON		{ $$ = $1; }
|	func_call SEMICOLON		{ $$ = $1; }
|	proc_call SEMICOLON		{ $$ = $1; }
|	if_stmt				{ $$ = $1; }
|	while_stmt			{ $$ = $1; }
|	do_while_stmt SEMICOLON		{ $$ = $1; }
|	func_def			{ $$ = def_yystype; }
|	switch_stmt			{ $$ = $1; }
|	label_def			{ $$ = $1; }
|	for_stmt			{ $$ = $1; }
;
label_def:
	IDENTIFIER COLON
	{
		$$ = def_yystype;
		$$[0] = get_label_vertex (get_constant ($1[0]), true);
	}
;
switch_prefix:
	SWITCH_KEYWD braced_logic_expr
	{
		push_break_vertex ();
		$$ = $2;
	}
;
switch_label:
	CASE_KEYWD arithm_single COLON { $$ = $2; }
|	DEFAULT_KEYWD COLON { $$ = def_yystype; }
;
func_def_prefix:
	FUNCTION_KEYWD IDENTIFIER
	{
		$$ = $2;
		set_current_func (get_constant ($2[0]));
	}
;
func_def:
	func_def_prefix body
	{
		string func_name = get_constant ($1[0]);
		pt t = connect (get_func_start (func_name), $2[0]);
		t = connect (t, get_func_end (func_name));
		$$ = def_yystype;
		reset_current_func ();
	}
;
switch_stmt:
	switch_prefix OCBRACE switch_lines ECBRACE
	{
		$$ = def_yystype;
		if ($3.size ()) {
			assert ($1.size ());

			if (is_constant ($1[0]) && get_constant ($1[0]) == "@menu") {
				$1[0] = get_last_menuint ();
			}

			$$[0] = connect (make_vertex (""), $1[0]);
			pt ev = pop_break_vertex ();

			for (int i = 0; i + 2 < $3.size (); ++i)
				connect ($3[i], $3[i + 2]);

			for (int i = 0; i < $3.size (); i += 2) {
				if (!is_menup ($1[0])) {
					connect ($$[0], $3[i]);
				} else {
					int id = get_constant_int ($3[i + 1]);
					if (id != -1)
						connect (get_menu_choice ($1[0], id, true), $3[i]);
				}
			}

			connect ($3[$3.size () - 2], ev);
//			$$[0] = connect ($$[0], $3[0]);
			$$[0] = connect ($$[0], ev);
		}
	}
;
switch_lines:
	/* empty */			{ $$ = YYSTYPE (); }
|	switch_lines switch_label lines 
	{
		$$ = $1;
		assert ($2.size () == 1);
		assert ($3.size () == 1);
		assert (!is_vertexp ($2[0]));
		$$.insert ($$.end (), $3.begin (), $3.end ());
		$$.insert ($$.end (), $2.begin (), $2.end ());
	}
;
assign_expr:
	IDENTIFIER ASSIGN_OP logic_expr	{ $$ = $3; }
;
while_prefix:
	WHILE_KEYWD
	{
		push_break_vertex ();
	}
;
while_stmt:
	while_prefix braced_logic_expr body
	{
		$$ = def_yystype;
		$$[0] = connect ($2[0], $3[0]);
		connect ($$[0], $$[0]);
		$$[0] = connect ($$[0], pop_break_vertex ());
	}
;
for_prefix:
	FOR_KEYWD
	{
		push_break_vertex ();
	}
;
for_stmt:
	for_prefix OBRACE assign_expr SEMICOLON logic_expr SEMICOLON arithm_expr EBRACE body
	{
		$$ = def_yystype;
		$$[0] = connect ($3[0], $5[0]);
		$$[0] = connect ($$[0], $7[0]);

		pt ev = pop_break_vertex ();

		pt t = connect ($$[0], $9[0]);
		connect (t, t);
		$$[0] = connect ($$[0], ev);
	}
;
do_while_prefix:
	DO_KEYWD
	{
		push_break_vertex ();
	}
;
do_while_stmt:
	do_while_prefix body while_prefix braced_logic_expr
	{
		$$ = def_yystype;
		$$[0] = connect ($2[0], $4[0]);
		connect ($$[0], $$[0]);
		pop_break_vertex (); // for while_prefix
		$$[0] = connect ($$[0], pop_break_vertex ());
	}
;
braced_logic_expr:
	OBRACE logic_expr EBRACE	{ $$ = $2; }
;
proc_call:
	IDENTIFIER proc_args
	{
		$$ = def_yystype;

		string proc_name = get_constant ($1[0]);

		if (proc_name == "close") {
			$$[0] = make_goto_vertex (get_end_vertex ());
		} else

		if (proc_name == "selectd")
			proc_name = "select";
		if (proc_name == "menu" || proc_name == "menuint" || proc_name == "select") {
			$$[0] = process_menu (false, proc_name, $2);
		} else

		if (proc_name == "goto") {
			if ($2.size () >= 1 && is_constant ($2[0])) {
				$$[0] = get_label_vertex (get_constant ($2[0]), false);
				$$[0] = make_goto_vertex ($$[0]);
			} else
				fprintf (stderr, "warn: `goto` has no valid argument\n");
		} else

		if (proc_name == "break") {
			if (is_vertexp (top_break_vertex ()))
				$$[0] = make_goto_vertex (top_break_vertex ());
			else
				fprintf (stderr, "warn: `break` has no valid context\n");
		} else

		if (proc_name == "return") {
			if (is_vertexp (get_current_func_end ()))
				$$[0] = make_goto_vertex (make_ret_vertex ());
			else
				fprintf (stderr, "warn: `return` called outside of a function\n");
		} else

		{
			for (int i = 0; i < $2.size (); ++i) {
				if (!is_vertexp ($2[i]))
					continue;

				if (proc_name == "speech" || proc_name == "mesq" || proc_name == "npctalk3")
					mark_as_npc ($2[i]);

				if (proc_name == "narrator" || proc_name == "mes")
					mark_as_narrator ($2[i]);

				if (proc_name == "select")
					mark_as_player ($2[i]);
			}

			for (int i = 0; i < $2.size (); ++i) {
				$$[0] = connect ($$[0], $2[i]);
			}

			$$[0] = connect ($$[0], make_func_call (proc_name));
		}
	}
;
%%
