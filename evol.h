#ifndef _EVOL_H_
#define _EVOL_H_

#include <bits/stdc++.h>
using namespace std;

#define YYSTYPE vector < pair <int, int> >
typedef pair <int, int> pt;
extern pt def_const;
extern YYSTYPE def_yystype;
extern string script_name;

pt make_constant (string s, int lineno = -1);

pt make_vertex (string caption = "", int flags = 0, int lineno = -1);
pt get_start_vertex ();

void make_edge (int u, int v);

bool is_vertexp (const pt& x);

pt connect (const pt& f, const pt& s);

bool is_constant (const pt &x);

string get_constant (const pt &x, int *lineno = NULL);
int get_constant_int (const pt &x);

void mark_as_npc (pt v);
void mark_as_player (pt v);
void mark_as_narrator (pt v);

pt get_end_vertex ();
bool has_target_edge (pt v);

void remove_vertex (int v, bool save_edges = true);
void finalize_script ();

#endif
