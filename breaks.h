#ifndef _BREAKS_H_
#define _BREAKS_H_

#include "evol.h"

pt top_break_vertex ();
pt pop_break_vertex ();
void push_break_vertex ();
void clear_breaks ();

#endif
