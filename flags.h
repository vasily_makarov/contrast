#ifndef _FLAGS_H_
#define _FLAGS_H_

#include "evol.h"

const int FL_SHOWN =	(1 << 0);
const int FL_NPC =	(1 << 1);
const int FL_NARRATOR = (1 << 2);
const int FL_PLAYER =	(1 << 3);
const int FL_GOTO =	(1 << 4);
const int FL_FUNCV_S =	(1 << 5);
const int FL_FUNCV_E =	(1 << 6);
const int FL_FCALL = 	(1 << 7);
const int FL_FRET = 	(1 << 8);
const int FL_NOTSHOWN = (1 << 14);
const int FL_BAD = 	(1 << 15);
const int FL_REMOVED =	(1 << 16);
const int FL_DONTREMOVE =	(1 << 17);

int get_flags (int v);
int get_flags (pt v);

void set_flags (int v, int flags);
void set_flags (pt v, int flags);

void add_flags (int v, int flags);
void add_flags (pt v, int flags);

void remove_flags (int v, int flags);
void remove_flags (pt v, int flags);

bool has_flags (int v, int flags);
bool has_flags (pt v, int flags);

bool has_any_flag (int v, int flags);
bool has_any_flag (pt v, int flags);

void clear_flags ();

#endif
