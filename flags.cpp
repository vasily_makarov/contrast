#include "flags.h"

int vf[1000 * 1000];

int get_flags (int v) {
	return vf[v];
}

int get_flags (pt v) {
	return get_flags (v.first);
}

void set_flags (int v, int flags) {
	vf[v] = flags;
}

void set_flags (pt v, int flags) {
	set_flags (v.first, flags);
}

void add_flags (int v, int flags) {
	vf[v] |= flags;
}

void add_flags (pt v, int flags) {
	add_flags (v.first, flags);
}

void remove_flags (int v, int flags) {
	vf[v] &= ~flags;
}

void remove_flags (pt v, int flags) {
	remove_flags (v.first, flags);
}

bool has_flags (int v, int flags) {
	return (vf[v] & flags) == flags;
}

bool has_flags (pt v, int flags) {
	return has_flags (v.first, flags);
}

bool has_any_flag (int v, int flags) {
	return (vf[v] & flags) > 0;
}

bool has_any_flag (pt v, int flags) {
	return has_any_flag (v.first, flags);
}

void clear_flags () {
	memset (vf, 0, sizeof (vf));
}
