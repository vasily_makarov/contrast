#include "classes.h"
#include "flags.h"
#include "funccalls.h"

string get_classes (pt v) {
	string result = "";
	if (has_flags (v, FL_NPC))
		result += "talk-npc ";
	if (has_flags (v, FL_PLAYER))
		result += "talk-player ";
	if (has_flags (v, FL_NARRATOR))
		result += "talk-narrator ";
	if (has_flags (v, FL_NOTSHOWN))
		result += "not-shown ";
	if (has_any_flag (v, FL_NPC | FL_PLAYER | FL_NARRATOR | FL_NOTSHOWN))
		result += "not-translated ";
	if (has_flags (v, FL_FUNCV_S))
		result += "func-start ";
	if (has_flags (v, FL_FCALL))
		result += "func-call ";
	if (has_flags (v, FL_FRET))
		result += "func-ret ";
	if (v == get_start_vertex ())
		result += "start ";
	return result;
}

string get_params (const pt& v, const string& datastr, int lineno) {
	static char __param_buf[10000];
	char *ptr = __param_buf;
	*ptr = 0;
	ptr += sprintf (ptr, "\t{data: {id: 'v%d', string: %s, ", v.first, datastr.c_str ());
	if (has_any_flag (v, FL_FUNCV_S | FL_FCALL))
		ptr += sprintf (ptr, "func: '%s', ", get_func_name (v).c_str ());
	if (lineno != -1)
		ptr += sprintf (ptr, "lineno: '%d', ", lineno);
	ptr += sprintf (ptr, "}, ");
	ptr += sprintf (ptr, "classes: '%s', ", get_classes (v).c_str ());
	return string (__param_buf);
}
