$(function () {
	function updateNodeTranslation (node) {
		string = node.data ('string');
		id = transifexGetStringId (string);

		node.data ('strid', id);

		if (transifexStringHasTranslation (id)) {
			node.removeClass ('not-translated');
			node.addClass ('translated');
		} else {
			node.removeClass ('translated');
			node.addClass ('not-translated');
		}
	}

	$('#btnGetStrings').click (function () {
		transifexLoadStrings (
				$('#textApiKey').val (),
				$('#textLangSlug').val (), 
				function (response) {
					alert ('Strings loaded successfully!');
					cy.$('node.not-translated', 'node.translated').forEach (
							updateNodeTranslation
					);
				},
				function (response) {
					alert ('Failed to load strings: ' + response.responseText);
				}
		);
	});

	var selStrid = -1;

	$('#btnUpdTranslation').click (function () {
		if (!transifexStringIdIsCorrect (selStrid))
			return;

		var tran = $('#areaTranslation').val ();
		transifexUpdateStringTranslation (selStrid, tran,
			function (strid, response) {
				cy.nodes ('[strid = ' + strid + ']').forEach (
					updateNodeTranslation
				);
			},
			function (response) {
				alert ('Failed to update translation: ' + response.responseText);
			}
		);
	});

	cy.nodes ('.translated, .not-translated').on ('tap', function (evt) {
		var str = this.data ('string');
		var strid = this.data ('strid');
		var lineno = this.data ('lineno');
		


		selStrid = strid;

		$('#areaOriginal').val (str);
		$('#areaTranslation').val (transifexGetStringTranslation (strid));

		if (lineno != undefined) {
			$('#lineLink').attr ('href', $('#scriptPath').attr ('href') + '#L' + lineno);
			$('#lineLink').text ('line ' + lineno);
		} else {
			$('#lineLink').text ('');
		}
	});
});
