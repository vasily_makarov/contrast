var tfxStrings;

var tfxLoadSuccess;
var tfxApikey;
var tfxLang;

function transifexLoadStrings (apikey, lang, onSuccess = null, onError = null) {
	tfxLoadSuccess = false;
	tfxApikey = apikey;
	tfxLang = lang;

	$.ajax({
		type: "GET",
		url: "https://www.transifex.com/api/2/project/evol/resource/serverdata-beta/translation/" + lang + "/strings",
		headers: {
			"Authorization": "Basic " + btoa ("api:" + apikey)
		},
		data: "",
		success: function (response) {
			tfxLoadSuccess = true;
			tfxStrings = response;
			if (onSuccess)
				onSuccess (response);
		}
	}).fail (function (response) {
		tfxStrings = [];
		if (onError)
			onError (response);
	});
}

function transifexUpdateStringTranslation (strid, tran, onSuccess, onError) {
	if (!tfxLoadSuccess)
		return false;

	if (!transifexStringIdIsCorrect (strid))
		return false;

	var hash = hex_md5 ([tfxStrings[strid].key, ''].join (':'));

	$.ajax({
		type: "PUT",
		url: "https://www.transifex.com/api/2/project/evol/resource/serverdata-beta/translation/" + tfxLang + "/string/" + hash,
		contentType: "application/json",
		headers: {
			"Authorization": "Basic " + btoa ("api:" + tfxApikey)
		},
		data: '{"translation": "' + tran + '"}',
		success: function (response) {
			tfxStrings[strid].translation = tran;
			if (onSuccess)
				onSuccess (strid, response);
		}
	}).fail (function (response) {
		if (onError)
			onError (response);
	});

	return true;
}

function transifexGetStringId (string) {
	for (var i = 0; i < tfxStrings.length; ++i) {
		var s = tfxStrings[i];
		if (s.source_string == string)
			return i;
	}
	return -1;
}

function transifexStringIdIsCorrect (id) {
	return 0 <= id && id < tfxStrings.length;
}

function transifexGetStringTranslation (id) {
	if (transifexStringIdIsCorrect (id))
		return tfxStrings[id].translation;
	return "";
}

function transifexStringHasTranslation (id) {
	return transifexGetStringTranslation (id) != "";
}

function transifexGetString (id) {
	if (transifexStringIdIsCorrect (id))
		return tfxStrings[id].source_string;
	return "";
}
