$(function(){
	var cy = window.cy = cytoscape({
		container: document.getElementById('cy'),
		boxSelectionEnabled: false,
		autounselectify: true,
		layout: {
			name: 'dagre'
		},
		style: [
			{
				selector: 'node',
				style: {
					'font-weight': 'bold',
					'content': 'data(caption)',
					'text-valign': 'center',
					'font-size': 10,
					'text-halign': 'center',
					'color': '#ffffff',
					'background-color': '#cccccc'
				}
			},
			{
				selector: 'edge',
				style: {
					'width': 2,
					'target-arrow-shape': 'triangle',
					'source-arrow-shape': 'circle',
					'line-color': '#9dbaea',
					'target-arrow-color': '#9dbaea',
					'source-arrow-color': '#9dbaea',
					'curve-style': 'bezier',
					'background-color': '#cccccc'
				}
			},
			{
				selector: 'edge.highlighted',
				style: {
					'width': 4,
					'line-color': '#0000ff',
					'target-arrow-color': '#0000ff',
					'source-arrow-color': '#0000ff',
				}
			},
			{
				selector: 'node.talk-npc',
				style: {
					'background-color': '#00ff00'
				}
			},
			{
				selector: 'node.talk-player',
				style: {
					'background-color': '#ffff00'
				}
			},
			{
				selector: 'node.talk-narrator',
				style: {
					'background-color': '#8888ff'
				}
			},
			{
				selector: 'node.not-shown',
				style: {
					'background-color': '#ff8800',
					'content': '?',
					'font-size': 27
				}
			},
			{
				selector: 'node.not-translated',
				style: {
					'border-style': 'solid',
					'border-color': '#ff0000',
					'border-width': 5
				}
			},
			{
				selector: 'node.not-translated',
				style: {
					'border-style': 'solid',
					'border-color': '#ff0000',
					'border-width': 5
				}
			},
			{
				selector: 'node.translated',
				style: {
					'border-style': 'solid',
					'border-color': '#008000',
					'border-width': 5
				}
			},
			{
				selector: 'node.func-start',
				style: {
					'content': 'data(func)',
					'text-valign': 'top',
					'color': '#000000',
					'background-color': '#000000'
				}
			},
			{
				selector: 'node.func-call',
				style: {
					'content': 'CALL',
					'background-color': '#000000'
				}
			},
			{
				selector: 'node.func-ret',
				style: {
					'content': 'RET',
					'background-color': '#000000'
				}
			},
			{
				selector: 'node.start',
				style: {
					'content': 'START',
					'background-color': '#000000',
					'border-color': '#000000',
					'border-width': 40,
				}
			},
			{
				selector: 'node.highlighted',
				style: {
					'border-color': '#cccccc',
					'border-width': 40
				}
			},
		],
		elements: {
			nodes: _cy_nodes,
			edges: _cy_edges
		},
	});

	cy.nodes().forEach (function (elem) {
		if (elem.data('string') != "") {
			elem.qtip({
				content: elem.data('string'),
				position: {
					my: 'top center',
					at: 'bottom center'
				},
				style: {
					classes: 'qtip-bootstrap',
					tip: {
						width: 16,
						height: 8
					}
				}
			});
		}
	});

	cy.nodes().on('mouseover', function() {
		this.outgoers(function () {
			if (this.isEdge ())
				this.addClass ('highlighted');
		});
	}).on('mouseout', function() {
		this.outgoers(function () {
			if (this.isEdge ())
				this.removeClass ('highlighted');
		});
	});


	cy.nodes('.func-call').on('mousedown', function() {
		var func = this.data('func');
		cy.nodes('.func-start[func = "' + func + '"]').forEach (function (elem) {
			elem.addClass ('highlighted');
		});
	}).on('mouseup', function() {
		var func = this.data('func');
		cy.nodes('.func-start[func = "' + func + '"]').forEach (function (elem) {
			elem.removeClass ('highlighted');
		});
	}).on('cxttap', function() {
		var func = this.data('func');
		cy.nodes('.func-start[func = "' + func + '"]').forEach (function (elem) {
			elem.addClass ('highlighted');
			cy.animate ({
				center: {
					eles: elem
				},
				zoom: 2.0,
				complete: function() {
					setTimeout (
							function() {
								elem.removeClass('highlighted');
							},
							500
					);
				}
			});
		});
	});

	$('#scriptName').text (_script_name);
	if (typeof _script_path != 'undefined') {
		$('#scriptPath').text (_script_path);
		$('#scriptPath').attr ('href', 'https://gitlab.com/evol/serverdata/blob/master/npc/' + _script_path);
	}
});
